"""
file name: calculate_P_F.py
language: Python2.7
date: 10/11/2020
author: Matthew Arran
email: arran@ipgp.fr
Reads .basalforce file from a specified simulation and extracts
- the power spectral density P_F of the base-normal force exerted on the base
- the moving average of P_F over a frequency range rt(g / d)
"""

import sys
import numpy as np
import pandas as pd

basal_suffix = '.basalforce'
output_suffix = '.P_F'

# Define geometric resolution of downsampled output
d_log2_f = 0.01
# Define gravitational accleration (m/s^2), particle diameter (m)
g, d = 9.8, 2e-3

# Attempt to read specified simulation's .basalforce file
try:
    print('Analysing {}'.format(sys.argv[1] + basal_suffix))
except FileNotFoundError:
    print('{} not found. Exiting...'.format(sys.argv[1] + basal_suffix))
    exit()
sys.stdout.flush()

# If skiprows argument specified, parse the number of initial rows to skip
skiprows = 0
if len(sys.argv) > 2:
    for arg in sys.argv[2:]:
        if (arg[:9] == 'skiprows='):
            try:
                skiprows = int(arg[9:])
            except ValueError:
                print('Cannot parse {} an integer. Exiting...'.format(arg[9:]))
        else:
            print('{} not recognised as an argument')

# Determine number of columns, in order to read the first and final three
data = pd.read_csv(sys.argv[1] + basal_suffix, sep=' ', nrows=2)
usecols = [0, data.shape[1] - 3, data.shape[1] - 2, data.shape[1] - 1]
# Read data
data = pd.read_csv(sys.argv[1] + basal_suffix, sep=' ', skiprows=skiprows,
                   usecols=usecols, names=['t', 'Fx', 'Fy', 'Fz'])

# Calculate and write mean force and effective friction
F_n = data.Fz.mean()
mu = -data.Fx.mean() / F_n
with open(sys.argv[1] + output_suffix, 'w') as output_file:
    output_file.write('F_n {F_n:11.5e} mu {mu:8.6f}\n'
                      .format(F_n=F_n, mu=mu))

# Use time data to set up Fourier transforms, before clearing it from memory 
n_t = data.t.size
Dt = (data.t.iloc[-1] - data.t.iloc[0])
nfft = int(2**np.floor(np.log2(n_t)))    
data.t = None
# Define frequency domain of data and of downsampled output
f = np.arange(nfft) * n_t / (2 * nfft * Dt)
id_st = np.argmax(f > np.sqrt(g / d))
n_smooth = int(1 / Dt * np.sqrt(g / d) / f[1])
f_i = f[1] * 2**np.arange(0, np.log2(nfft - id_st - n_smooth), d_log2_f * Dt)

# Calculate power spectrum of downslope force, moving average, and downsample 
P_F = np.abs(np.fft.fft(data.Fx, n=2*nfft)[:nfft] / n_t)**2 * Dt
P_F_bar = np.cumsum(P_F[id_st:])
P_F_bar = ((P_F_bar[2*n_smooth:] - P_F_bar[:-2*n_smooth])
           / (2 * n_smooth))
P_Fx = np.interp(f_i, f, P_F)
P_Fx_bar = np.interp(f_i, f[id_st + n_smooth:-n_smooth], P_F_bar,
                     left=np.nan, right=np.nan)
# Calculate power spectrum of cross-slope force, moving average, and downsample
P_F = np.abs(np.fft.fft(data.Fy, n=2*nfft)[:nfft] / n_t)**2 * Dt
P_F_bar = np.cumsum(P_F[id_st:])
P_F_bar = ((P_F_bar[2*n_smooth:] - P_F_bar[:-2*n_smooth])
           / (2 * n_smooth))
P_Fy = np.interp(f_i, f, P_F)
P_Fy_bar = np.interp(f_i, f[id_st + n_smooth:-n_smooth], P_F_bar,
                     left=np.nan, right=np.nan)
# Calculate power spectrum of base-normal force, moving average, and downsample
P_F = np.abs(np.fft.fft(data.Fz, n=2*nfft)[:nfft] / n_t)**2 * Dt
P_F_bar = np.cumsum(P_F[id_st:])
P_F_bar = ((P_F_bar[2*n_smooth:] - P_F_bar[:-2*n_smooth])
           / (2 * n_smooth))
P_Fz = np.interp(f_i, f, P_F)
P_Fz_bar = np.interp(f_i, f[id_st + n_smooth:-n_smooth], P_F_bar,
                     left=np.nan, right=np.nan)

# Write output
df = pd.DataFrame({'f': f_i, 'P_Fx': P_Fx, 'P_Fy': P_Fy, 'P_Fz': P_Fz,
		   'P_Fx_bar': P_Fx_bar, 'P_Fy_bar': P_Fy_bar, 'P_Fz_bar': P_Fz_bar})

df.to_csv(sys.argv[1] + output_suffix, mode='a',
          columns=['f', 'P_Fx', 'P_Fx_bar', 'P_Fy', 'P_Fy_bar', 'P_Fz', 'P_Fz_bar'],
          index=False, sep=' ',
          float_format='%11.5e', doublequote=False)
