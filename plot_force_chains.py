import numpy as np
from itertools import product
from mpl_toolkits import mplot3d
from mpl_toolkits.mplot3d.art3d import Line3DCollection
from matplotlib.cm import get_cmap
from matplotlib.colors import ListedColormap
import matplotlib.pyplot as plt

sim_id = 'th24.5h08'
nx, ny, nz = 10, 10, 10
n_frames = 400
F_opaque, F_max = 80, 400

# Define density (kg/m^3), gravity (m/s^2), particle diameter (m)
rho = 2500
g = 9.81
d = 0.002

# Define file locations
data_folder = './Archive_data/'
plot_folder = './Analysis/Figures/'
simulation_name = f'RunSteady/10dx10d/slow_{sim_id}/RunSteady'
plot_name = lambda fid: f'force_chains/{sim_id}/frame{fid:03d}.png'

cmap = get_cmap('viridis')
n_opaque = int(cmap.N * F_opaque / F_max)
alpha_cmap = cmap(np.arange(cmap.N))
alpha_cmap[:n_opaque,-1] = np.linspace(0, 1, n_opaque)
alpha_cmap = ListedColormap(alpha_cmap)

# Read files produced by the simulation
with open(data_folder + simulation_name + '.data') as data_file,\
     open(data_folder + simulation_name + '.fstat') as force_file:
    # Read data corresponding to each timestep
    for f_id in range(n_frames):
        data_header = data_file.readline().split()
        if not data_header:
            break
        N, t = int(data_header[0]), float(data_header[1])
        # Read particle positions and radii from datafile
        positions = np.empty([N, 3])
        radii = np.empty(N)
        for p_id in range(N):
            data_strings = data_file.readline().split()
            positions[p_id] = np.array(data_strings[:3], dtype=float) / d
            radii[p_id] = float(data_strings[6]) / d
        # Save first timestep
        if f_id == 0:
            t0, positions0 = t, positions
        # Read list of the particles in contact and the force between them 
        force_segments = []
        forces = []
        for _ in range(3):
            force_header = force_file.readline()
        force_strings = force_file.readline().split()
        while force_strings and (force_strings[0] != '#'):
            id_i, id_j = int(force_strings[1]), int(force_strings[2])
            Fn = float(force_strings[8]) / (rho * g * d**3)
            position_i = positions[id_i]
            if id_j == -1:
                # Handle contact between a particle and the lower boundary
                position_j = position_i - np.array([0, 0, radii[id_i]])
                max_sep = radii[id_i]
            else:
                # Handle contact between particles
                position_j = positions[id_j]
                max_sep = radii[id_i] + radii[id_j]
            # Check whether contact is in domain or across a periodic boundary
            separation = np.abs(position_i - position_j)
            if (separation**2).sum() > max_sep**2:
                # Handle contacts across a periodic boundary:
                # ... calculate the positions of periodic copies
                offset = (np.array([nx, ny, 0])
                          * (separation > max_sep)
                          * np.sign(position_i - position_j))
                copy_i, copy_j = position_i - offset, position_j + offset
                # ... calculate out-of-domain fraction of a force chain segment
                prop = (1 / (1 - (position_j / copy_i)[np.argmin(copy_i)])
                        if copy_i.min() < 0 else
                        1 / (1 - (copy_j / position_i)[np.argmin(copy_j)]))
                # ... calculate where the segments intersect the boundary
                boundary_i = (1 - prop) * copy_i + prop * position_j
                boundary_j = prop * copy_j + (1 - prop) * position_i
                # ... add force chain segments
                force_segments.append([boundary_i, position_j])
                forces.append(Fn)
                force_segments.append([position_i, boundary_j])
                forces.append(Fn)
            else:
                force_segments.append([position_i, position_j])
                forces.append(Fn)
            force_strings = force_file.readline().split()
        # Collate force chain segments
        force_segments = np.array(force_segments)
        forces = np.array(force)
        force_chains = Line3DCollection(force_segments, array=forces,
                                        cmap=alpha_cmap)
        force_chains.set_clim(0, F_max)
        # Plot force chains in 3D axes
        fig = plt.figure()
        ax = fig.add_axes([0.05, 0.05, 0.8, 0.9], projection='3d',
                          xlim=[0, nx], ylim=[0, nz], zlim=[0, nz])
        network = ax.add_collection(force_chains)
        ax.set_xlabel(r'$x / d$')
        ax.set_ylabel(r'$y / d$')
        ax.set_zlabel(r'$z / d$')
        time = (t - t0) / (rho * g * d**3)
        fig.text(0.5, 0.9, rf'$(t - t_0) \sqrt{{g / d}} = {time:7.5f}$',
                 fontsize=12)
        cax = fig.add_axes([0.875, 0.1, 0.025, 0.8])
        fig.colorbar(network, cax)
        cax.set_ylabel(r'$F_n / \rho g d^3$')
        fig.savefig(plot_folder + plot_name(f_id))
        plt.close()
        
displacements = []
for s_x, s_y in product(range(-1,2), repeat=2):
    copies = positions - np.array([s_x * nx, s_y * ny, 0])
    displacements.append(np.sqrt(np.sum((copies - positions0)**2, axis=1)))

print('Maximum particle displacement: {}'
      .format(np.array(displacements).min(axis=0).max()))
