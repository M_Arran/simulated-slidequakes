"""
file name: calculate_wall_force.py
language: Python2.7
date: 10/11/2020
author: Matthew Arran
email: arran@ipgp.fr
Reads .basalforce file from a specified simulation with walls and extracts
- the mean forces applied on the boundaries
"""

import sys
import numpy as np
import pandas as pd

basal_suffix = '.basalforce'
output_suffix = '.F_boundary'

# Attempt to read specified simulation's .F_boundary file
try:
    print('Analysing {}'.format(sys.argv[1] + basal_suffix))
except FileNotFoundError:
    print('{} not found. Exiting...'.format(sys.argv[1] + basal_suffix))
    exit()
sys.stdout.flush()

data = pd.read_csv(sys.argv[1] + basal_suffix, sep = ' ',
                   usecols=[1,2,3,4,5,6,7,8,9],
                   names=['wall_Fx', 'wall_Fy', 'wall_Fz',
                          'edge_Fx', 'edge_Fy', 'edge_Fz',
                          'centre_Fx', 'centre_Fy', 'centre_Fz'])

# Calculate and print mean forces on channel boundaries
data.mean().to_csv(sys.argv[1] + output_suffix,
                   float_format='%11.5e')
